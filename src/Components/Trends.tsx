import {FeedContent} from './Feed';

let TrendTitle = ({title, icon}: FeedContent) => {
  return (
    <div className="trendHeader d-flex">
      <h5 className="titleTrend">{title}</h5>
      <i className={icon}></i>
    </div>
  );
};
let TrendContent = ({body}: FeedContent) => {
  return (
    <>
      <h5 className="trendPostBody">{body}</h5>
    </>
  );
};
export const Trends = () => {
  return (
    <div className="trendContentList ">
      <h3>Trends for you</h3>
      <div className="trendPost">
        <TrendTitle title={'Trending in Sport'} icon={'fas fa-ellipsis-h'} />
        <TrendContent body={'LA Lakers wins against the Suns'} />
      </div>
      <div className="trendPost">
        <TrendTitle title={'Trending in World'} icon={'fas fa-ellipsis-h'} />
        <TrendContent body={'War in Ukraine'} />
      </div>
      <div className="trendPost">
        <TrendTitle title={'Trending in Weather'} icon={'fas fa-ellipsis-h'} />
        <TrendContent body={'Sunny in Bitola '} />
      </div>
      <div className="trendPost">
        <TrendTitle title={'Trending in Food'} icon={'fas fa-ellipsis-h'} />
        <TrendContent body={'10 Best Pizza Recipes'} />
      </div>
    </div>
  );
};
