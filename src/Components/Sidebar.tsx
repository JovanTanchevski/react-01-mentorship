type SidebarList = {
  text: string;
  icon: any;
};
type SidebarItems = SidebarList[];

const sidebarListItems: SidebarItems = [
  {text: 'Home', icon: <i className="fa-solid fa-house"></i>},
  {text: 'Explore', icon: <i className="fa-solid fa-hashtag"></i>},
  {text: 'Notifications', icon: <i className="fa-solid fa-bell"></i>},
  {text: 'Messages', icon: <i className="fa-solid fa-envelope"></i>},
  {text: 'Bookmarks', icon: <i className="fa-solid fa-bookmark"></i>},
  {text: 'Lists', icon: <i className="fa-solid fa-list"></i>},
  {text: 'Profile', icon: <i className="fa-solid fa-user"></i>},
  {text: 'More', icon: <i className="fa-solid fa-ellipsis"></i>},
];

export const Sidebar = () => {
  return (
    <div className="sideBar">
      <ul>
        <i className="fa-brands fa-twitter fa-2x mb-4"></i>
        {sidebarListItems.map(({text, icon}) => (
          <li key={`${text}-${icon}`}>
            {icon} <a href="#">{text}</a>
          </li>
        ))}
        <button className="tweetBtn">Tweet</button>
      </ul>
    </div>
  );
};
