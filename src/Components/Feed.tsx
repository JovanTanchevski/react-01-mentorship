import {useEffect, useState} from 'react';
import {IconType} from 'react-icons';

export type FeedContent = {
  title?: string;
  body?: string;
  icon?: string;
};
let FeedType = ({title, body}: FeedContent) => {
  return (
    <div className="postCard">
      <h3>{title}</h3>
      <p>{body}</p>
    </div>
  );
};
export const Feed = () => {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    const fetchPosts = async () => {
      const response = await fetch(
        'https://jsonplaceholder.typicode.com/posts'
      );
      const result = await response.json();
      setPosts(result);
    };
    fetchPosts();
  }, []);

  return (
    <div className="Feed">
      {posts.map((post: FeedContent) => (
        <FeedType
          key={`${post.title}-${post.body}`}
          title={post.title}
          body={post.body}
        />
      ))}
    </div>
  );
};
