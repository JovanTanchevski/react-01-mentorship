import React from 'react';
import './App.css';
import {Feed} from './Components/Feed';
import {Sidebar} from './Components/Sidebar';
import {Trends} from './Components/Trends';

function App() {
  return (
    <div className="mainDiv">
      <Sidebar />
      <Feed />
      <Trends />
    </div>
  );
}

export default App;
